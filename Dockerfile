FROM quay.io/mattbot/base:ubuntu-18.04-1

RUN apt-get update \
    && apt-get install \
      bash \
      curl \
      git \
      ncurses-bin \
      unzip

RUN version=0.11.7 shasum='6b8ce67647a59b2a3f70199c304abca0ddec0e49fd060944c26f666298e23418' \
 && cd /tmp \
 && curl -L -o ./terraform.zip "https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip" \
 && sha256sum ./terraform.zip | grep -q "${shasum}" \
 && unzip ./terraform.zip \
 && install ./terraform /usr/local/bin/

COPY ./entrypoint.sh /bin/_entrypoint
ENTRYPOINT ["/sbin/tini", "-g", "--", "/bin/_entrypoint"]

VOLUME /terraform
WORKDIR /terraform

CMD ["help"]
