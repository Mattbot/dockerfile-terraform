# Terraform
...but in a Container!

Updates to this Dockerfile will trigger an automated image build at [quay.io][quay.io].

That image may be downloaded in docker like so:

```docker pull quay.io/mattbot/terraform```

[quay.io]: https://quay.io
